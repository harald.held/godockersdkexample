package main

import (
	"bytes"
	"context"
	"flag"
	"io"
	"log"
	"os"
	"path/filepath"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/stdcopy"
	"github.com/docker/go-connections/nat"
	"github.com/mholt/archiver/v3"
)

const (
	imageName     = "simple_server_go"
	containerName = "simpleServerFromGo"
)

func tarDirectory(dir string) (io.Reader, error) {
	tar := archiver.NewTar()

	var buf bytes.Buffer

	err := tar.Create(&buf)

	if err != nil {
		return nil, nil
	}

	defer tar.Close()

	err = filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if path == dir {
			return nil
		}

		if err != nil {
			log.Printf("error walking the build context dir: %+v", err)

			return err
		}

		f, _ := os.Open(path)
		defer f.Close()

		err = tar.Write(archiver.File{
			FileInfo: archiver.FileInfo{
				FileInfo: info,
			},
			ReadCloser: f,
		})

		return err
	})

	if err != nil {
		return nil, err
	}

	return &buf, nil
}

func buildImage(dockerFilePath, buildContextDir string) error {
	ctx := context.Background()

	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())

	if err != nil {
		return err
	}

	buildContextReader, _ := tarDirectory(buildContextDir)

	// this shows how to inject any arguments into the image
	buildArgs := make(map[string]*string)
	vv := "AHA"
	buildArgs["A_VAR_VALUE"] = &vv

	resp, err := cli.ImageBuild(ctx, buildContextReader, types.ImageBuildOptions{
		Remove:     true,
		Tags:       []string{imageName},
		Dockerfile: dockerFilePath,
		BuildArgs:  buildArgs,
	})

	if err != nil {
		return err
	}

	io.Copy(os.Stdout, resp.Body)

	return nil
}

func runContainer(arg string) error {
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}

	resp, err := cli.ContainerCreate(ctx, &container.Config{
		Image: "simple_server_go",
		Cmd:   []string{arg},
	}, &container.HostConfig{
		AutoRemove: true,
		PortBindings: nat.PortMap{
			"12345/tcp": []nat.PortBinding{
				{
					HostIP:   "0.0.0.0",
					HostPort: "12345",
				},
			},
		},
	}, nil, containerName)

	if err != nil {
		panic(err)
	}

	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		panic(err)
	}

	out, err := cli.ContainerLogs(ctx, resp.ID, types.ContainerLogsOptions{ShowStdout: true})
	if err != nil {
		panic(err)
	}

	stdcopy.StdCopy(os.Stdout, os.Stderr, out)

	return nil
}

func main() {
	buildImageRequired := flag.Bool("buildImageRequired", false, "set if the Docker image should be (re-)built")

	flag.Parse()

	if *buildImageRequired {
		err := buildImage("Dockerfile", "./simpleServer")

		if err != nil {
			panic(err)
		}
	}

	err := runContainer("\"arbitray string here\"")

	if err != nil {
		panic(err)
	}
}

package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	valueFromEnvVar := os.Getenv("A_VAR_VALUE")

	var valueFromArg string

	if len(os.Args) > 1 {
		valueFromArg = os.Args[1]
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, _ = fmt.Fprint(w, "Hello from simple server: "+valueFromEnvVar+" ... "+valueFromArg)
	})

	log.Fatal(http.ListenAndServe(":12345", nil))
}
